//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var secondsPassed = 0
    var totalTime = 0
    
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var progressbarview: UIProgressView!
    
    let eggs = ["Soft": 300,"Medium":420,"Hard":720]
    var timer: Timer?
    
    @IBOutlet weak var subTitleLabel: UILabel!
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        timer?.invalidate()
        
        guard let hardness = sender.currentTitle else {
            print("current title is nil")
            return }
        
        totalTime = eggs[hardness] ?? 0
        progressbarview.progress = 0.0
        secondsPassed = 0
        titlelabel.text = hardness
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if(secondsPassed < totalTime) {
            secondsPassed += 1
            let percentage = Float(secondsPassed) / Float(totalTime)
            subTitleLabel.text = String(secondsPassed)
            progressbarview.progress = percentage
        }else{
            timer?.invalidate()
            subTitleLabel.text = ""
            titlelabel.text = "Done"
        }
    }
}
